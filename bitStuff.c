/* Program for bit stuffing and bit de-stuffing
*/

#include<stdio.h>
#include<conio.h>
#include<string.h>

int main(){
    char str1[50] = {"01111110"}, str2[50];
    char input[40]; //input as array of characters
    int a, i = 8, j, counter = 0, k;
    
    //input data
    printf("Enter the data: ");
    scanf("%s", input); 
    
    //bit stuffing loop
    for(a = 0; a < strlen(input); a++){
        if(input[a] == '1')
            counter++;
        else
            counter = 0;
        
        //getting the stuffed data	
        str1[i++] = input[a];
        
        if(counter == 5){
            str1[i++] = '0';
            counter = 0;
        }

    }
    strcat(str1, "01111110");
    //printing stuffed data
    printf("\nBit stuffed data: ");
    for(j = 0; j < i+8; j++){
        printf("%c", str1[j]);
    }

    //bit destuffing
    counter = 0;
    for(j = 8, k = 0; j < i+8; j++){
        if(str1[j] == '1')
            counter++;
        else
            counter = 0;
        
        //getting the destuffed data
        str2[k++] = str1[j];
            
        if(counter == 6)
            break;
        else if((counter == 5) && str1[j+1] == '0'){
            j++;
            counter = 0;
        }
           
    }
    
    //printing destuffed data
    printf("\n\nBit de-stuffed data: ");
    for(j = 0; j < k - strlen("01111110") + 1; j++)
        printf("%c", str2[j]);
    return 0;
}